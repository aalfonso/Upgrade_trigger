## Summary

Scripts to deal with upgrade trigger studies. SCripts to generate samples using Moore (with) ganga

## Structure

**Dictionaries**: Information about tree location in ntuples, and project repository key name. <br />
**OptionFiles**: Contains Option Files for DaVinci and ganga.<br />
**plots**: Where the plots are saved.<br />
**Python-scripts**: Python scripts not for DaVinci/ganga.<br />
**Shell-scripts**: Bash scripts for different purposes.<br />
**Variables**: Contains lists of variables, selection cuts...<br />
**Tuples**: Folder with ntupes.<br />
**preliminary**: This folder contains all code, when Moore was not ready yet and there was a preliminary repository to test and upload the lines.<br />


## Notes

Extra folder named **Tuples** where .root files are stored is frequently accessed thoughout the code. plots folder is often the default location for plots.<br />

## Author

Alejandro Alfonso Albero (alejandro.alfonso.albero@cern.ch)