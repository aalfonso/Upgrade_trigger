Selection efficiencies applied on Tuples/Extrasel_HHgamma.root: 

                     Preselection                     
------------------------------------------------------
Upsilon_BKGCAT > 30

          Efficiency for each cut, and global          
-------------------------------------------------------
Upsilon_VtxChi2 < 10           |  0.976925
Upsilon_DIRA_OWNPV > 0.999     |  0.135709
Upsilon_IP_OWNPV < 0.2         |  0.975097
Upsilon_IPCHI2_OWNPV < 8       |  0.606384
log(Upsilon_FDCHI2_OWNPV) > 3  |  0.217403
log(Upsilon_FD_OWNPV) > 1      |  0.0937041
piplus_PT > 400                |  0.521427
Global                         |  0.00688665
