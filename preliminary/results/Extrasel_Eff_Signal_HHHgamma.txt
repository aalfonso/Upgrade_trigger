Selection efficiencies applied on Tuples/Extrasel_HHHgamma.root: 

                     Preselection                     
------------------------------------------------------
Upsilon_BKGCAT == 30

          Efficiency for each cut, and global          
-------------------------------------------------------
Upsilon_VtxChi2 < 10           |  1
Upsilon_DIRA_OWNPV > 0.999     |  0.434783
Upsilon_IP_OWNPV < 0.2         |  1
Upsilon_IPCHI2_OWNPV < 8       |  0.826087
log(Upsilon_FDCHI2_OWNPV) > 3  |  0.130435
log(Upsilon_FD_OWNPV) > 1      |  0.391304
piplus_PT > 400                |  0.826087
Global                         |  0.0434783
