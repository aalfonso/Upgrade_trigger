Selection efficiencies applied on Tuples/Extrasel_HHHgamma.root: 

                     Preselection                     
------------------------------------------------------
Upsilon_BKGCAT > 30

          Efficiency for each cut, and global          
-------------------------------------------------------
Upsilon_VtxChi2 < 10           |  0.988936
Upsilon_DIRA_OWNPV > 0.999     |  0.204096
Upsilon_IP_OWNPV < 0.2         |  0.993404
Upsilon_IPCHI2_OWNPV < 8       |  0.855745
log(Upsilon_FDCHI2_OWNPV) > 3  |  0.0935106
log(Upsilon_FD_OWNPV) > 1      |  0.077234
piplus_PT > 400                |  0.531436
Global                         |  0.00414894
