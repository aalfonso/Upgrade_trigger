## Summary

Scripts to deal with upgrade trigger studies

## Structure

**Dictionaries**: Information about tree location in ntuples, and project repository key name. <br />
**OptionFiles**: Contains Option Files for DaVinci and ganga.<br />
**plots**: Where the plots are saved.<br />
**Python-scripts**: Python scripts not for DaVinci/ganga.<br />
**Shell-scripts**: Bash scripts for different purposes.<br />
**Variables**: Contains lists of variables, selection cuts...<br />
**Tuples**: Folder with ntupes.<br />


## Notes

Extra folder named **Tuples** where .root files are stored is frequently accessed thoughout the code. plots folder is often the default location for plots.<br />


## Makefile test explanation

It is possible, from the ntuples produced from the trigger preselection to produce the efficiency curves for the BDT. The required steps are as follow:<br />
**Parameters**: Parameters for each line are numbered from 1 to 4 (HHgamma, HHHgamma, HHgammaEE, HHHGammaEE). The **y** parameters set the **background** rate (in kHz) at which the BDT outputs if the efficiency is 1. The **x** parameters set the preselection efficiency on **signal**, computed with MCmatched events. The **oldrate** parameter is for the event rate (in kHz) obtained from the Run 2 BDT, while **oldeff** is for its signal efficiency (in MCmatched events).<br />
**Flow**: The location of the ntuples must be saved in a text file, by default it is saved on the **Directories** repository (../Directories/Trigger_$(Channel)_$(line)Line.dir), where Channel should refer to the signal type and line to the trigger line analyzed. These get applied BKGCAT cuts, saved in the **Variables** folder, and then written into Tuples/$(Channel)_BKGCAT_Line$(line).root. Furthermore, they are also mixed and applied only safe cuts to remove infinities and saved into Tuples/Signal_Line$(line).root. Now comes the **signal** and **background** preparation, the signal is taken from the previously BKGCAT cut tuples, applied safecuts and saved into Tuples/BDTcuttreeMC.root, whereas the background, whose locations are saved in Trigger_MinBias_$(line)Line.dir in the **Directories** folder, is directly applied safecuts and saved into Tuples/BDTcuttreebkg.root. Finally, the BDT is trained on these samples and then is applied to them. An automatic scan of the efficiencies of the BDT is done, plus a plot comparing the new efficiencies to the old ones.<br />
**Checks**: Apart from checking efficiency plot, one must check the the signal without MCBKG cuts is being well-reproduced.<br />


## Makefile schematics
Raw signal files ---MCBKG cuts---> MCmatched signal files ---Safecuts---> BDT sig input
Raw signal files ---Safecuts---> Cross-check signal files
Raw bkg files ---Safecuts---> BDT bkg input

BDT sig/bkg input ---BDT---> Efficiencies ---Plot---> Comparison with Run II

## Author

Alejandro Alfonso Albero (alejandro.alfonso.albero@cern.ch)