import json
import sys

def GetNames(fdir):
    DSTs = []
    with open(fdir) as file:
        for line in file:
            DSTs.append(line.replace("\n",""))
    file.close()
    return DSTs

def analyse(fnames):
    ntotal = 0;
    npassed = 0;
    for fname in fnames:
        with open(fname) as f:
            print(fname)
            statistics = json.load(f)

        metadata = statistics.pop('metadata')
        ntotal += metadata['ntotal']
        npassed += metadata['npassed']
    print "Total events"
    print ntotal
    print "Passed events"
    print npassed

if __name__ == '__main__':
    fname = sys.argv[-1]
    analyse(GetNames(fname))
