"""Example options file for running over microDST output of the Stripping job.

To adapt this to your needs, change the Stripping line name and the various
decay descriptors, then change the path to the input file you'd like to use.

As in any other Stripping job, line candidates are saved to:

    /Event/<stream name>/Phys/<Stripping line name>/Particles

The output of each line-specific extra selection is saved to:

    /Event/<stream name>/Phys/<Stripping line name>/<Extra selection name>/Particles

Run the DaVinci job as you would normally, e.g. from inside this directory:

    $ ../run gaudirun.py ntuples.py
"""
from Configurables import CombineParticles, SubstitutePID
from PhysSelPython.Wrappers import Selection, DataOnDemand, SelectionSequence
from Configurables import (
    DaVinci,
    DecayTreeTuple
)
from DecayTreeTuple import Configuration

#Returns Selection using CombineParticles algorithm
def combineSelection(name,**kwargs):
    if 'DaughtersCuts' not in kwargs: kwargs['DaughtersCuts'] = {}
    if 'Preambulo' not in kwargs: kwargs['Preambulo'] = []
    alg = CombineParticles(
        'Combine'+name,
        DecayDescriptors=kwargs['DecayDescriptors'],
        CombinationCut=kwargs['CombinationCut'],
        MotherCut=kwargs['MotherCut'],
        DaughtersCuts=kwargs['DaughtersCuts'],
        Preambulo=kwargs['Preambulo']
        )
    return Selection(
        'Selection'+name,
        Algorithm=alg,
        RequiredSelections=kwargs['RequiredSelections']
        )

# The name of the Stripping line of interest
line_name = 'LineHHgamma'
extrasel = 'Selection_HHgamma_PVExtraSelection'

#Obtain B0 selection and extra pion selection
sel_strip = DataOnDemand(Location='/Phys/{0}/Particles'.format(line_name))
sel_extra = DataOnDemand(Location='/Phys/{0}/{1}/Particles'.format(line_name, extrasel))

#We have to build B0 + pi
Selection_B0pi = combineSelection(
    '_B0_pi', #Return with Selection in name prefix
    DecayDescriptors=['B+ -> B0 pi+', 'B- -> B0 pi-'],
    CombinationCut="APT > 0",
    MotherCut="ALL",
    RequiredSelections=[sel_strip, sel_extra]
    
)
Seq_B0pi = SelectionSequence('SelSeq_B0_pi', TopSelection=Selection_B0pi)

#Build DecayTreeTuple
dtt = DecayTreeTuple('CandidatesFrom{0}'.format(line_name))
dtt.Inputs = [Seq_B0pi.outputLocation()]
# Replace these with the decay and branch descriptors of interest
dtt.Decay = '(B+ -> ^(B0 -> ^(K*(892)0 -> ^K+ ^K-) ^gamma) ^pi+) || (B- -> ^(B0 -> ^(K*(892)0 -> ^K+ ^K-) ^gamma) ^pi-)'
dtt.addBranches({
    'B0'     : '(B+ -> ^(B0 -> (K*(892)0 -> K+ K-) gamma) pi+) || (B- -> ^(B0 -> (K*(892)0 -> K+ K-) gamma) pi-)',
    'kst'    : '(B+ -> (B0 -> ^(K*(892)0 -> K+ K-) gamma) pi+) || (B- -> (B0 -> ^(K*(892)0 -> K+ K-) gamma) pi-)',
    'Kplus'  : '(B+ -> (B0 -> (K*(892)0 -> ^K+ K-) gamma) pi+) || (B- -> (B0 -> (K*(892)0 -> ^K+ K-) gamma) pi-)',
    'Kminus' : '(B+ -> (B0 -> (K*(892)0 -> K+ ^K-) gamma) pi+) || (B- -> (B0 -> (K*(892)0 -> K+ ^K-) gamma) pi-)',
    'gamma'  : '(B+ -> (B0 -> (K*(892)0 -> K+ K-) ^gamma) pi+) || (B- -> (B0 -> (K*(892)0 -> K+ K-) ^gamma) pi-)',
    'piplus' : '(B+ -> (B0 -> (K*(892)0 -> K+ K-) gamma) ^pi+) || (B- -> (B0 -> (K*(892)0 -> K+ K-) gamma) ^pi-)',
    'Upsilon': '(B+ -> (B0 -> (K*(892)0 -> K+ K-) gamma) pi+) || (B- -> (B0 -> (K*(892)0 -> K+ K-) gamma) pi-)'
})
dtt.ToolList += [
#"TupleToolMCTruth"
 "TupleToolMCBackgroundInfo"
, "TupleToolAngles"
, "TupleToolDira"
, "TupleToolGeometry"
, "TupleToolKinematic"
, "TupleToolTrackInfo"]
#Custom vars
from Configurables import LoKi__Hybrid__TupleTool
LoKiTool = LoKi__Hybrid__TupleTool( 'LoKiTool')

dtt.addTool(LoKiTool, name = 'LoKiTool')
dtt.ToolList += [
"LoKi::Hybrid::TupleTool/LoKiTool"
]
from LoKiCore.math import log10
dtt.LoKiTool.Variables = { 
    'VtxChi2'      : "VFASPF(VCHI2/VDOF)",
}

#DaVinci().appendToMainSequence([Seq_B0pi])
DaVinci().UserAlgorithms = [Seq_B0pi, dtt]
DaVinci().InputType = 'DST'
DaVinci().DataType = 'Upgrade'
DaVinci().Simulation = True
DaVinci().TupleFile = '{0}.root'.format(dtt.getName())
