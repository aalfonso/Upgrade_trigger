#proxy
#ganga bd_ganga.py
app = GaudiExec()
app.directory   = "/afs/cern.ch/work/a/aalfonso/private/upgrade-bandwidth-studies"
app.useGaudiRun =  True
app.options     = ['/afs/cern.ch/work/a/aalfonso/private/Upgrade_trigger/OptionFiles/caca_strip.py',
'/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r353/options/DaVinci/DataType-Upgrade.py',
'/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r353/options/DaVinci/Simulation.py',
'/afs/cern.ch/work/a/aalfonso/private/Upgrade_trigger/OptionFiles/ntuples.py',
'/afs/cern.ch/work/a/aalfonso/private/Upgrade_trigger/OptionFiles/data_cond.py']

#UNFILTERED
#dataset = BKQuery(path='/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/12203224/LDST').getDataset() #CHANGE!!
#HLT1
dataset = BKQuery(path='/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/12203224/LDST').getDataset() #CHANGE!!



j = Job(application=app)
j.application.platform = 'x86_64-slc6-gcc62-opt'
j.backend = Dirac()
#j.backend.settings['BannedSites'] = ['LCG.GRIDKA.de']
j.name = 'K1Gam Trigger' #CHANGE!!
j.comment = "Filter HLT1" #CHANGE!!
j.inputdata = dataset
j.inputfiles = ['/afs/cern.ch/work/a/aalfonso/private/upgrade-bandwidth-studies/Phys/StrippingSelections/python/StrippingSelections/StrippingRadInclusive/TMVA_HHgammaLine.weights.xml',
                '/afs/cern.ch/work/a/aalfonso/private/upgrade-bandwidth-studies/Phys/StrippingSelections/python/StrippingSelections/StrippingRadInclusive/TMVA_HHgammaEELine.weights.xml',
                '/afs/cern.ch/work/a/aalfonso/private/upgrade-bandwidth-studies/Phys/StrippingSelections/python/StrippingSelections/StrippingRadInclusive/TMVA_HHHgammaLine.weights.xml',
                '/afs/cern.ch/work/a/aalfonso/private/upgrade-bandwidth-studies/Phys/StrippingSelections/python/StrippingSelections/StrippingRadInclusive/TMVA_HHHgammaEELine.weights.xml']

j.splitter = SplitByFiles(filesPerJob=10, ignoremissing = True)
j.outputfiles = [LocalFile("*.root"), LocalFile("*.json")]
j.submit()   



#hadd bla.root /{1..30}/output/rootname.root
