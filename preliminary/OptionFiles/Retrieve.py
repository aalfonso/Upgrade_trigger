import sys
import os
#Part to retrieve LFN directions
jobNumber = int(sys.argv[-1])
length = len(jobs(jobNumber).subjobs)
localdir = "../Upgrade_trigger/mDSTs/"+str(jobNumber)+"/"
known_name = False
filename = ""
for i in range(0,length):
    if(jobs(jobNumber).subjobs(i).status == 'completed'):
        if (known_name):
            if os.path.isfile(localdir+filename+"_"+str(i)):
                continue
        output = jobs(jobNumber).subjobs(i).backend.getOutputDataLFNs()
        if( output.hasLFNs() == True):
            print "Reading subjob number %d" % i
            arr = output.getReplicas().keys()
            arr.sort()
            df = DiracFile(lfn=arr[0])
            #Retrieve file name
            filename = arr[0].split("/")[-1]
            known_name = True
            df.localDir = localdir
            if not(os.path.isfile(localdir+filename+"_"+str(i))):
                try:
                    df.get()
                    os.rename(localdir+filename, localdir+filename+"_"+str(i))
                except:
                    pass

