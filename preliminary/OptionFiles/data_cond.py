"""Load some arbitrary upgrade MC (Bs To Ds K).

Taken from:
    evt+std://MC/Upgrade/13264031/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/LDST
"""

from Configurables import CondDB, DaVinci, Stripping

DaVinci().CondDBtag = 'sim-20171127-vc-md100'
DaVinci().DDDBtag = 'dddb-20171126'
