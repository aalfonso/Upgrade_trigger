"""Example options file for running over microDST output of the Stripping job.

To adapt this to your needs, change the Stripping line name and the various
decay descriptors, then change the path to the input file you'd like to use.

As in any other Stripping job, line candidates are saved to:

    /Event/<stream name>/Phys/<Stripping line name>/Particles

The output of each line-specific extra selection is saved to:

    /Event/<stream name>/Phys/<Stripping line name>/<Extra selection name>/Particles

Run the DaVinci job as you would normally, e.g. from inside this directory:

    $ ../run gaudirun.py ntuples.py
"""
from Configurables import (
    DaVinci,
    DecayTreeTuple
)
from DecayTreeTuple import Configuration
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import TupleToolSubMass
from LoKiCore.math import log10

# The name of the Stripping line of interest
line_name = 'LineHHgamma'

dtt = DecayTreeTuple('CandidatesFrom{0}'.format(line_name))
dtt.Inputs = ['Phys/{0}/Particles'.format(line_name)]
# Replace these with the decay and branch descriptors of interest
dtt.Decay = 'B0 -> ^(K*(892)0 -> ^K+ ^K-) ^gamma'
dtt.addBranches({
    'B0': 'B0 -> (K*(892)0 -> K+ K-) gamma',
    'kst' : 'B0 -> ^(K*(892)0 -> K+ K-)  gamma',
    'Kplus': 'B0 -> (K*(892)0 -> ^K+ K-) gamma',
    'Kminus': 'B0 -> (K*(892)0 -> K+ ^K-) gamma',
    'gamma': 'B0 -> (K*(892)0 -> K+ K-) ^gamma'
})
toollist = [
    #"TupleToolMCTruth"
    "TupleToolMCBackgroundInfo"
    , "TupleToolAngles"
    , "TupleToolDira"
    , "TupleToolGeometry"
    , "TupleToolKinematic"
    , "TupleToolTrackInfo"]
dtt.ToolList += toollist
#Custom vars
LoKiTool1 = LoKi__Hybrid__TupleTool( 'LoKiTool1')

dtt.addTool(LoKiTool1, name = 'LoKiTool1')
dtt.ToolList += [
    "LoKi::Hybrid::TupleTool/LoKiTool1"
]

dtt.LoKiTool1.Variables = { 
    'ipchi2'      : "log10(BPVIPCHI2())",
    'ipchi2_min'  : "log10(MINTREE(BPVIPCHI2(), ((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0'))))",
    'gamma_pt'    : "CHILD(2, PT)/MeV",
    'm_corrected' : "BPVCORRM",
    'vm_corrected': "CHILD(1, BPVCORRM)",
    'fdchi2'      : "log10(BPVVDCHI2)",
    'vtx_chi2'    : "log10(VFASPF(VCHI2))",
    'doca'        : "CHILD(1, DOCA(1,2))"
}
LoKi_Kst=dtt.kst.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Kst")
LoKi_Kst.Variables =  {
    "doca": 'DOCA(1,2)'
}



####HHH gamma
# The name of the Stripping line of interest
line_name2 = 'LineHHHgamma'
    
dtt2 = DecayTreeTuple('CandidatesFrom{0}'.format(line_name2))
dtt2.Inputs = ['/Event/Phys/{0}/Particles'.format(line_name2)]
# Replace these with the decay and branch descriptors of interest
dtt2.Decay = '(B+ -> ^(D*(2010)+ -> ^(K*(892)0 -> ^K+ ^K-) ^K+) ^gamma) || (B- -> ^(D*(2010)- -> ^(K*(892)0 -> ^K+ ^K-) ^K-) ^gamma)'
dtt2.addBranches({
    'B0'   : '( B+ -> (D*(2010)+ -> (K*(892)0 -> K+ K-) K+) gamma) || ( B- -> (D*(2010)- -> (K*(892)0 -> K+ K-) K-) gamma)',
    'Dst'  : '(B+ -> ^(D*(2010)+ -> (K*(892)0 -> K+ K-) K+) gamma) || (B- -> ^(D*(2010)- -> (K*(892)0 -> K+ K-) K-) gamma)',
    'kst'  : '(B+ -> (D*(2010)+ -> ^(K*(892)0 -> K+ K-) K+) gamma) || (B- -> (D*(2010)- -> ^(K*(892)0 -> K+ K-) K-) gamma)',
    'K1'   : '(B+ -> (D*(2010)+ -> (K*(892)0 -> ^K+ K-) K+) gamma) || (B- -> (D*(2010)- -> (K*(892)0 -> ^K+ K-) K-) gamma)',
    'K2'   : '(B+ -> (D*(2010)+ -> (K*(892)0 -> K+ ^K-) K+) gamma) || (B- -> (D*(2010)- -> (K*(892)0 -> K+ ^K-) K-) gamma)',
    'K3'   : '(B+ -> (D*(2010)+ -> (K*(892)0 -> K+ K-) ^K+) gamma) || (B- -> (D*(2010)- -> (K*(892)0 -> K+ K-) ^K-) gamma)',
    'gamma': '(B+ -> (D*(2010)+ -> (K*(892)0 -> K+ K-) K+) ^gamma) || (B- -> (D*(2010)- -> (K*(892)0 -> K+ K-) K-) ^gamma)'
})
#Mass substitution #############
dtt2.addTupleTool("TupleToolSubMass") #Mass subs
dtt2.TupleToolSubMass.SetMax = 4 #4-body. K+K-K+G
dtt2.TupleToolSubMass.Substitution += ["K- => pi- "]
dtt2.TupleToolSubMass.DoubleSubstitution += ["K+/K- => pi+/pi- "]
dtt2.TupleToolSubMass.DoubleSubstitution += ["K+/K+ => pi+/pi+ "]
################################
dtt2.ToolList += toollist
#Custom vars
LoKiTool2 = LoKi__Hybrid__TupleTool( 'LoKiTool2')

dtt2.addTool(LoKiTool2, name = 'LoKiTool2')
dtt2.ToolList += [
    "LoKi::Hybrid::TupleTool/LoKiTool2"
]

dtt2.LoKiTool2.Variables = { 
    'ipchi2'      : "log10(BPVIPCHI2())",
    'ipchi2_min'  : "log10(MINTREE(BPVIPCHI2(), ((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0'))))",
    'gamma_pt'    : "CHILD(2, PT)/MeV",
    'gamma_p'     : "CHILD(2, P)/MeV",
    'm_corrected' : "BPVCORRM",
    'fdchi2'      : "log10(BPVVDCHI2)",
    'vtx_chi2'    : "log10(VFASPF(VCHI2))",
    'chi2dof_max' : "MAXTREE(TRCHI2DOF, ISBASIC & HASTRACK & (ABSID=='K+'))"
}

#HHgammaEE
# The name of the Stripping line of interest
line_name3 = 'LineHHgammaEE'
    
dtt3 = DecayTreeTuple('CandidatesFrom{0}'.format(line_name3))
dtt3.Inputs = ['Phys/{0}/Particles'.format(line_name3)]
# Replace these with the decay and branch descriptors of interest
dtt3.Decay = 'B0 -> ^(K*(892)0 -> ^K+ ^K-) ^(gamma -> e+ e-)'
dtt3.addBranches({
    'B0': 'B0 -> (K*(892)0 -> K+ K-) (gamma -> e+ e-)',
    'kst' : 'B0 -> ^(K*(892)0 -> K+ K-)  (gamma -> e+ e-)',
    'Kplus': 'B0 -> (K*(892)0 -> ^K+ K-) (gamma -> e+ e-)',
    'Kminus': 'B0 -> (K*(892)0 -> K+ ^K-) (gamma -> e+ e-)',
    'gamma': 'B0 -> (K*(892)0 -> K+ K-) ^(gamma -> e+ e-)'
})
dtt3.ToolList += toollist
#Custom vars
LoKiTool3 = LoKi__Hybrid__TupleTool( 'LoKiTool3')

dtt3.addTool(LoKiTool3, name = 'LoKiTool3')
dtt3.ToolList += [
    "LoKi::Hybrid::TupleTool/LoKiTool3"
]
dtt3.LoKiTool3.Variables = { 
    'sumpt'       : "SUMTREE(PT, ((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')|(ABSID=='gamma')), 0.0)/MeV",
    'eta'         : "BPVETA",
    'minpt'       : "MINTREE(((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')|(ABSID=='gamma')), PT)/MeV",
    'nlt16'       : "NINTREE(((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')|(ABSID=='gamma')) & (BPVIPCHI2() < 16))",
    'ipchi2'      : "BPVIPCHI2()",
    'n1trk'       : "NINTREE(((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')|(ABSID=='gamma')) & (PT > 1*GeV) & (BPVIPCHI2() > 16))",
    'mcor'        : "BPVCORRM",
    'fdchi2'      : "BPVVDCHI2",
    'chi2'        : "VFASPF(VCHI2)"
}
    
#HHHgammaEE
# The name of the Stripping line of interest
line_name4 = 'LineHHHgammaEE'

dtt4 = DecayTreeTuple('CandidatesFrom{0}'.format(line_name4))
dtt4.Inputs = ['Phys/{0}/Particles'.format(line_name4)]
# Replace these with the decay and branch descriptors of interest
dtt4.Decay = '(B+ -> ^(D*(2010)+ -> ^(K*(892)0 -> ^K+ ^K-) ^K+) ^(gamma -> e+ e-)) || (B- -> ^(D*(2010)- -> ^(K*(892)0 -> ^K+ ^K-) ^K-) ^(gamma -> e+ e-))'
dtt4.addBranches({
    'B0'   : '( B+ -> (D*(2010)+ -> (K*(892)0 -> K+ K-) K+) (gamma -> e+ e-)) || ( B- -> (D*(2010)- -> (K*(892)0 -> K+ K-) K-) (gamma -> e+ e-))',
    'Dst'  : '(B+ -> ^(D*(2010)+ -> (K*(892)0 -> K+ K-) K+) (gamma -> e+ e-)) || (B- -> ^(D*(2010)- -> (K*(892)0 -> K+ K-) K-) (gamma -> e+ e-))',
    'kst'  : '(B+ -> (D*(2010)+ -> ^(K*(892)0 -> K+ K-) K+) (gamma -> e+ e-)) || (B- -> (D*(2010)- -> ^(K*(892)0 -> K+ K-) K-) (gamma -> e+ e-))',
    'K1'   : '(B+ -> (D*(2010)+ -> (K*(892)0 -> ^K+ K-) K+) (gamma -> e+ e-)) || (B- -> (D*(2010)- -> (K*(892)0 -> ^K+ K-) K-) (gamma -> e+ e-))',
    'K2'   : '(B+ -> (D*(2010)+ -> (K*(892)0 -> K+ ^K-) K+) (gamma -> e+ e-)) || (B- -> (D*(2010)- -> (K*(892)0 -> K+ ^K-) K-) (gamma -> e+ e-))',
    'K3'   : '(B+ -> (D*(2010)+ -> (K*(892)0 -> K+ K-) ^K+) (gamma -> e+ e-)) || (B- -> (D*(2010)- -> (K*(892)0 -> K+ K-) ^K-) (gamma -> e+ e-))',
    'gamma': '(B+ -> (D*(2010)+ -> (K*(892)0 -> K+ K-) K+) ^(gamma -> e+ e-)) || (B- -> (D*(2010)- -> (K*(892)0 -> K+ K-) K-) ^(gamma -> e+ e-))'
})
#Mass substitution #############
dtt4.addTupleTool("TupleToolSubMass") #Mass subs
dtt4.TupleToolSubMass.SetMax = 4 #4-body. K+K-K+G
dtt4.TupleToolSubMass.Substitution += ["K- => pi- "]
dtt4.TupleToolSubMass.DoubleSubstitution += ["K+/K- => pi+/pi- "]
dtt4.TupleToolSubMass.DoubleSubstitution += ["K+/K+ => pi+/pi+ "]
################################
dtt4.ToolList += toollist
#Custom vars
LoKiTool4 = LoKi__Hybrid__TupleTool( 'LoKiTool4')

dtt4.addTool(LoKiTool4, name = 'LoKiTool4')
dtt4.ToolList += [
    "LoKi::Hybrid::TupleTool/LoKiTool4"
]
dtt4.LoKiTool4.Variables = { 
    'sumpt'       : "SUMTREE(PT, ((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')|(ABSID=='gamma')), 0.0)/MeV",
    'eta'         : "BPVETA",
    'minpt'       : "MINTREE(((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')|(ABSID=='gamma')), PT)/MeV",
    'nlt16'       : "NINTREE(((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')|(ABSID=='gamma')) & (BPVIPCHI2() < 16))",
    'ipchi2'      : "BPVIPCHI2()",
    'n1trk'       : "NINTREE(((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')|(ABSID=='gamma')) & (PT > 1*GeV) & (BPVIPCHI2() > 16))",
    'mcor'        : "BPVCORRM",
    'fdchi2'      : "BPVVDCHI2",
    'chi2'        : "VFASPF(VCHI2)"
}

dtt.ErrorMax = 10    
dtt2.ErrorMax = 10    
dtt3.ErrorMax = 10    
dtt4.ErrorMax = 10    

############## ADD TISTOS INFO FOR HLT1 (IF NEEDED BE) ################
# Should HLT1 TIS/TOS information be added to the ntuple?
add_hlt1_tistos = True
if add_hlt1_tistos:
    triggerlist = [
        'Hlt1DiMuonHighMassDecision',
        'Hlt1DiMuonHighMassTightDecision',
        'Hlt1DiMuonLowMassDecision',
        'Hlt1DiMuonLowMassTightDecision',
        'Hlt1GlobalDecision',
        'Hlt1TrackMVALooseDecision',
        'Hlt1TrackMVATightDecision',
        'Hlt1TrackMVAVLooseDecision',
        'Hlt1TrackMVAVTightDecision',
        'Hlt1TrackMuonMVADecision',
        'Hlt1TwoTrackMVALooseDecision',
        'Hlt1TwoTrackMVATightDecision',
        'Hlt1TwoTrackMVAVLooseDecision',
        'Hlt1TwoTrackMVAVTightDecision'
        ]
    #HHgamma
    tistos = dtt.addTupleTool('TupleToolTISTOS')
    tistos.VerboseHlt1 = True
    tistos.FillL0 = False
    tistos.FillHlt2 = False
    tistos.TriggerList = triggerlist
    #HHHgamma
    tistos2 = dtt2.addTupleTool('TupleToolTISTOS')
    tistos2.VerboseHlt1 = True
    tistos2.FillL0 = False
    tistos2.FillHlt2 = False
    tistos2.TriggerList = triggerlist
    #HHgammaEE
    tistos3 = dtt3.addTupleTool('TupleToolTISTOS')
    tistos3.VerboseHlt1 = True
    tistos3.FillL0 = False
    tistos3.FillHlt2 = False
    tistos3.TriggerList = triggerlist
    #HHHgammaEE
    tistos4 = dtt4.addTupleTool('TupleToolTISTOS')
    tistos4.VerboseHlt1 = True
    tistos4.FillL0 = False
    tistos4.FillHlt2 = False
    tistos4.TriggerList = triggerlist
######################################################################

##############Add BDT response value to tuple#########################
add_BDT_response = True
if add_BDT_response:
    from MVADictHelpers import addTMVAclassifierTuple
    addTMVAclassifierTuple(dtt.B0, 'TMVA_HHgammaLine.weights.xml', dtt.LoKiTool1.Variables,
                           Name='BDT_response', Keep=False, Preambulo=[""])
    addTMVAclassifierTuple(dtt2.B0, 'TMVA_HHHgammaLine.weights.xml', dtt2.LoKiTool2.Variables,
                           Name='BDT_response', Keep=False, Preambulo=[""])
    addTMVAclassifierTuple(dtt3.B0, 'TMVA_HHgammaEELine.weights.xml', dtt3.LoKiTool3.Variables,
                           Name='BDT_response', Keep=False, Preambulo=[""])
    addTMVAclassifierTuple(dtt4.B0, 'TMVA_HHHgammaEELine.weights.xml', dtt4.LoKiTool4.Variables,
                           Name='BDT_response', Keep=False, Preambulo=[""])
######################################################################

DaVinci().UserAlgorithms += [dtt, dtt2, dtt3, dtt4]
DaVinci().InputType = 'MDST'
DaVinci().DataType = 'Upgrade'
DaVinci().Simulation = True
DaVinci().TupleFile = '{0}.root'.format(dtt.getName())
