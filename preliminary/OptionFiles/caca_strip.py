"""
This options file demonstrates how to run a stripping line
from a specific stripping version on a local MC DST file
It is based on the minimal DaVinci DecayTreeTuple example
"""

from Configurables import (
    EventNodeKiller,
    ProcStatusCheck,
    DaVinci
)

# Node killer: remove the previous Stripping
event_node_killer = EventNodeKiller('StripKiller')
event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

#Como se llama mi strema?
confname = 'RadInclusive'
#Import my new added lines and configure strema
from StrippingSelections import buildersConf
confs = buildersConf()
from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder

streams = buildStreamsFromBuilder(confs,confname)

#Configure stripping bruh
filterBadEvents = ProcStatusCheck()
from StrippingConf.Configuration import StrippingConf
sc = StrippingConf(Streams = streams,
                   MaxCandidates = 2000,
                   AcceptBadEvents = False,
                   BadEventSelection = filterBadEvents)

sc.sequence().ErrorMax = 10
DaVinci().appendToMainSequence([event_node_killer, sc.sequence()])
############################################
